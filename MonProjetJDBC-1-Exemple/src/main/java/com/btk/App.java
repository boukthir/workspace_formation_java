package com.btk;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

//import com.mysql.jdbc.Statement;

//import com.mysql.jdbc.Connection;
// java doc ***
public class App {
	public static void main(String[] args) {
		try {
		
			// chargement du connector
			Class.forName("com.mysql.jdbc.Driver");

			// connexion a la bd
			String url = "jdbc:mysql://localhost:3306/Banquedb";
			String user = "root";
			String password = "1234";
			Connection connection = DriverManager.getConnection(url, user, password);

			// execution de la requete
			// Statement statement ="select * from Comptes";

			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM COMPTES");

			// exploitation des res
			
			while(resultSet.next()) {
				String nom=resultSet.getString("Numero");
				String prop =resultSet.getString("proprietaire");
				String solde =resultSet.getString("solde");
				System.out.println("Compte"+nom+"- "+prop+ "-  "+solde);
			}
			
			// fermeture de la connexion
			connection.close();

		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			System.out.println("attention il ya une exception : "+e);

		}
	}
}
